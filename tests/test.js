const sentenceToUpperCase = require('..');

describe('sentenceToUpperCase', () => {
  it('should returns a boolean', async () => {
    const result = sentenceToUpperCase('test');

    expect(typeof result).toBe('string');
  });

  it('should returns only one uppercased word when only one word was passed as parameter', async () => {
    expect(isMaleName('Aga')).toEqual('AGA');
    expect(isMaleName('ala')).toEqual('ALA');
    expect(isMaleName('@L@')).toEqual('@L@');
    expect(isMaleName('neApolItAŃczykOWIANeczka')).toEqual('NEAPOLITAŃCZYKOWIANECZKA');
  });

  it('should returns sentence with uppercased every second word', async () => {
    expect(isMaleName('ala ma kota')).toEqual('ala MA kota');
    expect(isMaleName('aLa ma KOta i Psa')).toEqual('aLa MA KOta I Psa');
    expect(isMaleName('tester testowy')).toEqual('tester TESTOWY');
    expect(isMaleName('r@nd0M t3Xt')).toEqual('r@nd0M T3XT');
  });
});
